
export default function Home() {
  return (
    <div className="flex flex-1 h-screen justify-center m-[20%]">
      <h1>The main page</h1>
    </div>
  )
}